import { SkillsService } from './../../../shared/services/skills.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../../../shared/items/user';
import { UsersService } from './../../../shared/services/users.service';

@Component({
  selector: 'Cpt-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  skills: any;
  idUser: any;
  newPassword: any;
  currentUser: User;

  constructor(private service: UsersService,
              private serviceS: SkillsService
  ) {

  }

  ngOnInit() {
    this.serviceS.getSkill().subscribe((data: any) => this.skills = data );
  }

  changePassword() {
    let currentPassword = document.getElementById('currentPassword');
    let newPassword = document.getElementById('newPassword');
    let confirmPassword = document.getElementById('confirmPassword');
    let idUser = this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    if (currentPassword != null && newPassword != null && confirmPassword != null) {
      if (newPassword === confirmPassword) {
        this.service.updateUser(this.idUser, confirmPassword);
      }else {
        console.log('New password and Confirm password are different!');
      }
    }else {
      console.log('Error : There is a empty field!');
    }
  }



//  Disable fields where the associed radio button is not checked

  // disableFields() {
  //   const absentStatus = document.getElementById('absentStatus');
  //   const trainingStatus = document.getElementById('trainingStatus');

  //   if (absentStatus.checked) {
  //     document.getElementById('datetimePickerAbsentText').disabled = false;
  //     document.getElementById('datetimePickerAbsentButton').disabled = false;
  //     document.getElementById('datetimePickerTrainingText').disabled = true;
  //     document.getElementById('datetimePickerTrainingButton').disabled = true;
  //     document.getElementById('skillList').disabled = true;
  //   }else if (trainingStatus.checked) {
  //     document.getElementById('datetimePickerAbsentText').disabled = true;
  //     document.getElementById('datetimePickerAbsentButton').disabled = true;
  //     document.getElementById('datetimePickerTrainingText').disabled = false;
  //     document.getElementById('datetimePickerTrainingButton').disabled = false;
  //     document.getElementById('skillList').disabled = false;
  //   }else {
  //     document.getElementById('datetimePickerAbsentText').disabled = true;
  //     document.getElementById('datetimePickerAbsentButton').disabled = true;
  //     document.getElementById('datetimePickerTrainingText').disabled = true;
  //     document.getElementById('datetimePickerTrainingButton').disabled = true;
  //     document.getElementById('skillList').disabled = true;
  //   }
  // }

}
