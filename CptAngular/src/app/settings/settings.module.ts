import { SkillsService } from './../shared/services/skills.service';
import { UsersService } from './../shared/services/users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SettingsComponent } from './container/settings/settings.component';
import { SettingsRoutingModule } from './settings-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    HttpClientModule,
  ],
  providers: [UsersService, SkillsService],
  declarations: [SettingsComponent]
})
export class SettingsModule { }
