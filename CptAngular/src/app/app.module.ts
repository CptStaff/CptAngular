import { SettingsModule } from './settings/settings.module';
import { ProfilModule } from './profil/profil.module';
import { CreateCollaboratorManagementModule } from './create-collaborator-management/create-collaborator-management.module';
import { CollaboratorManagementModule } from './collaborator-management/collaborator-management.module';
import { LoginModule } from './login/login.module';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { CreateSkillManagementModule } from './create-skill-management/create-skill-management.module';
import { SkillManagementModule } from './skill-management/skill-management.module';
import { SkillValidationModule } from './skill-validation/skill-validation.module';


@NgModule({
  declarations: [
    AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CollaboratorManagementModule,
    CreateCollaboratorManagementModule,
    CreateSkillManagementModule,
    HomeModule,
    ProfilModule,
    SettingsModule,
    SkillManagementModule,
    SkillValidationModule,
    LoginModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
