import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaboratorManagementComponent } from './container/collaborator-management/collaborator-management.component';


const routes: Routes = [
  {path: 'collaborator_management', component: CollaboratorManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaboratorManagementRoutingModule { }
