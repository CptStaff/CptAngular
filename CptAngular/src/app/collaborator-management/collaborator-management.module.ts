import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollaboratorManagementComponent } from './container/collaborator-management/collaborator-management.component';
import { CollaboratorManagementRoutingModule } from './collaborator-management-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { UsersService } from '../shared/services/users.service';



@NgModule({
  imports: [
    CommonModule,
    CollaboratorManagementRoutingModule,
    HttpClientModule,
    ],
  providers: [UsersService],
  declarations: [CollaboratorManagementComponent]
})
export class CollaboratorManagementModule { }
