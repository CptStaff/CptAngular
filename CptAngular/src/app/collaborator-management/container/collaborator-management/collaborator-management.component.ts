
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { UsersService } from '../../../shared/services/users.service';


@Component({
  selector: 'Cpt-collaborator-management',
  templateUrl: './collaborator-management.component.html',
  styleUrls: ['./collaborator-management.component.css']
})
export class CollaboratorManagementComponent implements OnInit {
  editable: boolean = false;
  selectedRight: number;
  numUser: number;
  $users: any;
  users: any;
  changed: boolean = false;
  
  constructor(public service: UsersService) {

   }

  ngOnInit() {
    this.service.getUser().subscribe((data: any) => this.users = data );
  }
  edit(numUser) {
    this.numUser = numUser;
    this.editable = true;
  }
  setRight(right) {
    this.selectedRight = right.target.value;
  }
  okRight(idUser, newRight) {
    this.service.updateRight(idUser, newRight).subscribe(
      data => console.log('COOL : ', data),
      error => console.error('C\'est pas juste : ', error)
    );
    this.editable = false;
    this.changed = true;
  }
  cancelRight(right) {
    this.selectedRight = right;
    this.editable = false;
  }
}


