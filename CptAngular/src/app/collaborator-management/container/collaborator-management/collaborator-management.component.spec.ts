import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorManagementComponent } from './collaborator-management.component';

describe('CollaboratorManagementComponent', () => {
  let component: CollaboratorManagementComponent;
  let fixture: ComponentFixture<CollaboratorManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaboratorManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
