import { Component, OnInit } from '@angular/core';
import {ChartModule} from 'primeng/primeng';



@Component({
  selector: 'Cpt-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.css']
})
export class RadarComponent implements OnInit {
  data: any;
  constructor() {
    this.data = {
        labels: ['Java', 'C', 'C++', 'Javascript', 'Oral ease', 'Directing', 'Organizing'],
        datasets: [
            {
                label: 'My First dataset',
                backgroundColor: 'rgba(179,181,198,0.2)',
                borderColor: 'rgba(179,181,198,1)',
                pointBackgroundColor: 'rgba(179,181,198,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(179,181,198,1)',
                data: [5, 2, 3, 0, 3, 2, 3]
            }
        ]
    };
}


  ngOnInit() {
  }

}
