// tslint:disable-next-line:max-line-length
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../shared/services/users.service';
import { OwnsService } from '../../../shared/services/owns.service';

@Component({
  selector: 'Cpt-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.css']
})
export class HomeTableComponent implements OnInit {
  owns: any;
  users: any;

  constructor(public userService: UsersService, public ownsService: OwnsService) {

   }

  ngOnInit() {
    this.ownsService.getOwn().subscribe((data: any) => this.owns = data );
  }

}
