import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './container/home/home.component';
import { RadarComponent } from './components/radar/radar.component';
import { HomeTableComponent } from './components/home-table/home-table.component';
import { HomeRoutingModule } from './home-routing.module';
import { HttpClientModule } from '@angular/common/http';
import {ChartModule} from 'primeng/primeng';
import { UsersService } from '../shared/services/users.service';
import { OwnsService } from '../shared/services/owns.service';


@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    ChartModule

  ],
  providers: [UsersService, OwnsService],
  declarations: [HomeComponent, RadarComponent, HomeTableComponent]
})
export class HomeModule { }
