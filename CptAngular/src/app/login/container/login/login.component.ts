import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ViewEncapsulation } from '@angular/core/src/metadata/view';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../shared/items/user';
import { UsersService } from '../../../shared/services/users.service';


@Component({
  selector: 'Cpt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsersService, User]
})
export class LoginComponent implements OnInit {
  currentUser: User;
  data: any;
  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  login(event) {
    if (this.email.valid && this.password.valid) {
      // Run.
      this.dao.login(this.email.value, this.password.value).subscribe(
        data => (console.log(data), window.location.href = './profil',
        localStorage.setItem('currentUser', JSON.stringify(data))),
        error => console.error(error.status)
      );
    } else {
      console.error('Form errors');
    }
  }

  constructor(private dao: UsersService) {

   }

  ngOnInit() {
  }
}


