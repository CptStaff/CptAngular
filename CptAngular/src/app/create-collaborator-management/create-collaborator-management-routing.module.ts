import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateCollaboratorManagementComponent } from './container/create-collaborator-management/create-collaborator-management.component';



const routes: Routes = [
  {path: 'create_collaborator_management', component: CreateCollaboratorManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateCollaboratorManagementRoutingModule { }
