import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateCollaboratorManagementComponent } from './container/create-collaborator-management/create-collaborator-management.component';
import { CreateCollaboratorManagementRoutingModule } from './create-collaborator-management-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CreateCollaboratorManagementRoutingModule
  ],
  declarations: [CreateCollaboratorManagementComponent],
  exports: [CreateCollaboratorManagementComponent]
})
export class CreateCollaboratorManagementModule { }
