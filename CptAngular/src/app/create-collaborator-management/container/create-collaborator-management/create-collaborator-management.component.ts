import { UsersService } from './../../../shared/services/users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'Cpt-create-collaborator-management',
  templateUrl: './create-collaborator-management.component.html',
  styleUrls: ['./create-collaborator-management.component.css']
})
export class CreateCollaboratorManagementComponent implements OnInit {
  email: String;
  firstname: String;
  lastname: String;
  password = 'Password';
  right: number;

  constructor(public service: UsersService) {}

  ngOnInit() {
  }

 setFirstname(event) {
   this.firstname = event.target.value;
/*     console.log(this.firstname); */
  }
  setLastname(event) {
    this.lastname = event.target.value;
/*     console.log(this.lastname);
 */  }
  setEmail(event) {
    this.email = event.target.value;
/*     console.log(this.email);
 */  }
  setRight(right) {
    this.right = right.target.value;
  }
  createColl() {
    console.log(this.firstname, this.lastname, this.email, this.password, this.right);
  this.service.createUser(this.firstname, this.lastname, this.email, this.password, this.right).subscribe(
      data => console.log('COOL : ', data),
      error => console.error('C\'est pas juste : ', error)
    );
  }
}
