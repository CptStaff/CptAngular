import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCollaboratorManagementComponent } from './create-collaborator-management.component';

describe('CreateCollaboratorManagementComponent', () => {
  let component: CreateCollaboratorManagementComponent;
  let fixture: ComponentFixture<CreateCollaboratorManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCollaboratorManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCollaboratorManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
