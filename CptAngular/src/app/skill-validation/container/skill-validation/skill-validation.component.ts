import { UsersService } from './../../../shared/services/users.service';
import { User } from './../../../shared/items/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SkillValidationService } from '../../skill-validation.service';
import { TabsComponent } from '../../components/tabs/tabs.component';


@Component({
  selector: 'Cpt-skill-validation',
  templateUrl: './skill-validation.component.html',
  styleUrls: ['./skill-validation.component.css']
})

export class SkillValidationComponent implements OnInit {
  selectedUser: User;
  @ViewChild(TabsComponent) tabsComponent;
  users: any;

  constructor(public service: UsersService) { }

  ngOnInit() {
    this.service.getUser().subscribe((data: any) => this.users = data );
  }

  // Create a tab for the selected collaborator
  onChange(event) {
    const value = parseInt(event.target.value, 10);
    const found: Array<User> = this.users.filter(e => e.id === value);

    if (found.length === 1) {
      this.selectedUser = found[0];
      this.tabsComponent.openTab(this.selectedUser.firstname + ' ' + this.selectedUser.lastname, {}, true);
    } else {

    }
  }

}
