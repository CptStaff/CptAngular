import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillValidationComponent } from './skill-validation.component';

describe('SkillValidationComponent', () => {
  let component: SkillValidationComponent;
  let fixture: ComponentFixture<SkillValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
