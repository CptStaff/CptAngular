import { User } from './../../../shared/items/user';
import { Component, OnInit, Input } from '@angular/core';
import { OwnsService } from '../../../shared/services/owns.service';

@Component({
  selector: 'Cpt-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css'],
})
export class TabComponent implements OnInit {
  owns: any;
  @Input('tabTitle') title: string;
  @Input() active = false;
  @Input() isCloseable = false;
  @Input('userSelected') selectedUser: User;

  constructor(public service: OwnsService) { }

  ngOnInit() {
    this.service.getOwnByUser(this.selectedUser.id).subscribe((data: any) => this.owns = data );
  }

}
