import { TestBed, inject } from '@angular/core/testing';

import { SkillValidationService } from './skill-validation.service';

describe('SkillValidationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkillValidationService]
    });
  });

  it('should be created', inject([SkillValidationService], (service: SkillValidationService) => {
    expect(service).toBeTruthy();
  }));
});
