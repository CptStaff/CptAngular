import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillValidationComponent } from './container/skill-validation/skill-validation.component';

const routes: Routes = [
  {path: 'skill_validation', component: SkillValidationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkillValidationRoutingModule { }
