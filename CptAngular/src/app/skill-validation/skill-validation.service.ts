import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SkillValidationService {

  constructor(public http: HttpClient) { }

  getUser() {
    return this.http.get('http://localhost:8080/user/');
  }

}
