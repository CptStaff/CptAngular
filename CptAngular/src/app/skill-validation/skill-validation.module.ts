import { UsersService } from './../shared/services/users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SkillValidationRoutingModule } from './skill-validation-routing.module';
import { SkillValidationComponent } from './container/skill-validation/skill-validation.component';
import { TabComponent } from './components/tab/tab.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { DynamicTabsDirective } from './components/dynamic-tabs.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    SkillValidationRoutingModule,
    HttpClientModule,
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [UsersService],
  declarations: [SkillValidationComponent, TabComponent, TabsComponent, DynamicTabsDirective],
  entryComponents: [ TabComponent ]
})
export class SkillValidationModule { }
