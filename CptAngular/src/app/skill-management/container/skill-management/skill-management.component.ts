import { Component, OnInit } from '@angular/core';
import { OwnsService } from '../../../shared/services/owns.service';
// tslint:disable-next-line:max-line-length


@Component({
  selector: 'Cpt-skill-management',
  templateUrl: './skill-management.component.html',
  styleUrls: ['./skill-management.component.css']
})
export class SkillManagementComponent implements OnInit {
  owns: any;

  constructor(public service: OwnsService) { }

  ngOnInit() {
    this.service.getOwn().subscribe((data: any) => this.owns = data );
  }

}
