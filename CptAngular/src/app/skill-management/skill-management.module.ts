import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillManagementComponent } from './container/skill-management/skill-management.component';
import { SkillManagementRoutingModule } from './skill-management-routing.module';
import { OwnsService } from '../shared/services/owns.service';

@NgModule({
  imports: [
    CommonModule,
    SkillManagementRoutingModule,
  ],
  providers: [OwnsService],
  declarations: [SkillManagementComponent]
})
export class SkillManagementModule { }
