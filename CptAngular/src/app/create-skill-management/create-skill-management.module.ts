import { SkillsService } from './../shared/services/skills.service';
import { CreateSkillManagementRoutingModule } from './create-skill-management-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateSkillManagementComponent } from './container/create-skill-management/create-skill-management.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  imports: [
    CommonModule,
    CreateSkillManagementRoutingModule,
    HttpClientModule
  ],
  providers: [SkillsService],
  declarations: [CreateSkillManagementComponent]
})
export class CreateSkillManagementModule { }
