import { SkillsService } from './../../../shared/services/skills.service';
// tslint:disable-next-line:max-line-length
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'Cpt-create-skill-management',
  templateUrl: './create-skill-management.component.html',
  styleUrls: ['./create-skill-management.component.css']
})
export class CreateSkillManagementComponent implements OnInit {

  generalSkillTypeName: String;
  skillTypeName: String;
  skillName: String;
  skills: any;
  skillTypes: any;
  generalSkillTypes: any;
  generalSkillTypeId: number;
  skillTypeId: number;



  constructor(private service: SkillsService) {}

  ngOnInit() {
    this.service.getSkill().subscribe((data: any) => this.skills = data );
    this.service.getSkillType().subscribe((data: any) => this.skillTypes = data );
    this.service.getGeneralSkillType().subscribe((data: any) => this.generalSkillTypes = data );
  }

 setGeneralSkillTypeName(event) {
   this.generalSkillTypeName = event.target.value;
/*     console.log(this.firstname); */
  }
  setSkillTypeName(event) {
    this.skillTypeName = event.target.value;
/*     console.log(this.lastname);
 */  }
  setSkillName(event) {
    this.skillName = event.target.value;
/*     console.log(this.email);
 */  }

  createGeneralSkillType() {
    console.log(this.generalSkillTypeName);
  this.service.addGeneralSkillType(this.generalSkillTypeName).subscribe(
      data => console.log('data : ', data),
      error => console.error('err : ', error)
    );
  }

  createSkillType() {
    console.log(this.skillTypeName);
  this.service.addSkillType(this.skillTypeName, this.generalSkillTypeId).subscribe(
      data => console.log('data : ', data),
      error => console.error('err : ', error)
    );
  }

  createSkill() {
    console.log(this.skillName);
  this.service.addSkill(this.skillName, this.skillTypeId).subscribe(
      data => console.log('data : ', data),
      error => console.error('err : ', error)
    );
  }
}
