import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSkillManagementComponent } from './create-skill-management.component';

describe('CreateSkillManagementComponent', () => {
  let component: CreateSkillManagementComponent;
  let fixture: ComponentFixture<CreateSkillManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSkillManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSkillManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
