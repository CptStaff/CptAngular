import { CreateSkillManagementModule } from './create-skill-management.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateSkillManagementComponent } from './container/create-skill-management/create-skill-management.component';


const routes: Routes = [
  {path: 'create_skill_management', component: CreateSkillManagementComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateSkillManagementRoutingModule { }
