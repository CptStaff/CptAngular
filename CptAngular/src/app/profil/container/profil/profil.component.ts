import { User } from './../../../shared/items/user';
import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { OwnsService } from '../../../shared/services/owns.service';


@Component({
  selector: 'Cpt-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  changed: boolean = false;
  currentUser: User;
  newLevel: any;
  idOwn: any;
  owns: any;
  star: number;
  selectedStar;
  editable: boolean =false;
  numOwn: number = 0;

  constructor(public service: OwnsService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  ngOnInit() {
    let idUser = this.currentUser.id;
    this.service.getOwnByUser(idUser).subscribe((data: any) => this.owns = data );
  }
  onSelectionChange(star, starOwn) {
    this.selectedStar = star;
  }
  edit(numOwn) {
    this.numOwn = numOwn;
    this.editable = true;
  }
  okLevel(idOwn, newLevel) {
    this.service.updateLevel(idOwn, newLevel).subscribe(
      data => console.log('COOL : ', data),
      error => console.error('C\'est pas juste : ', error)
    );
    this.editable = false;
    this.changed = true;
  }

  cancelLevel(star) {
    this.selectedStar = star;
    this.editable = false;
  }
}