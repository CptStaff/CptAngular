import { ProfilRoutingModule } from './profil-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilComponent } from './container/profil/profil.component';
import { OwnsService } from '../shared/services/owns.service';

@NgModule({
  imports: [
    CommonModule,
    ProfilRoutingModule
  ],
  providers: [OwnsService],
  declarations: [ProfilComponent]
})
export class ProfilModule { }
