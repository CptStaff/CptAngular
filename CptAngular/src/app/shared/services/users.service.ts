import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';


@Injectable()
export class UsersService {
  root = 'http://localhost:8080/user/';
  constructor(public http: HttpClient) { }

  getUser() {
    return this.http.get('http://localhost:8080/user/');
  }

  createUser(firstname, lastname, email, password, right) {
    const uri = 'http://localhost:8080/user/';
    let params = new HttpParams();
    params = params.append('firstname', firstname)
      .append('lastname', lastname)
      .append('email', email)
      .append('password', password)
      .append('right', right);
    return this.http.post(uri, params);
  }
  private url(path: string) {
    return this.root + path;
  }

  private get(path: string) {
    return this.http.get(this.url(path));
  }

  login(email: string, password: string) {
    return this.get(`login?email=${email}&password=${password}`);
  }

  updateRight(idUser, newRight) {
    const uri = 'http://localhost:8080/user/' + idUser;
    let params = new HttpParams();
    params = params.append('right', newRight);
    return this.http.put(uri, params);
  }

  updateUser(idUser, newPassword) {
    const uri = 'http://localhost:8080/user/' + idUser;
    let params = new HttpParams();
    params = params.append('password', newPassword);
    return this.http.put(uri, params);
  }
}
