import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';

@Injectable()
export class SkillsService {
  constructor(public http: HttpClient) { }

  getSkill() {
    return this.http.get('http://localhost:8080/skill/');
  }

  
  getSkillType() {
    return this.http.get('http://localhost:8080/skill_types/');
  }

  
  getGeneralSkillType() {
    return this.http.get('http://localhost:8080/general_skill_types/');
  }

  addGeneralSkillType(generalSkillTypeName) {
    const uri = 'http://localhost:8080/general_skill_types/';
    let params = new HttpParams();
    params = params.append('name', generalSkillTypeName);
    return this.http.post(uri, params);
  }

  addSkillType(generalSkillTypeName, skillGTId) {
    const uri = 'http://localhost:8080/skill_types/';
    let params = new HttpParams();
    params = params.append('name', generalSkillTypeName)
                  .append('skillGTId', skillGTId );
    return this.http.post(uri, params);
  }

  addSkill(generalSkillTypeName, skillTId) {
    const uri = 'http://localhost:8080/skill/';
    let params = new HttpParams();
    params = params.append('name', generalSkillTypeName)
                  .append('skillGTId', skillTId );
    return this.http.post(uri, params);
  }
}
