import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';


@Injectable()
export class OwnsService {

  constructor(public http: HttpClient) { }

  getOwn() {
    return this.http.get('http://localhost:8080/own/');
  }

  getOwnByUser(idUser) {
    return this.http.get('http://localhost:8080/own/user/' + idUser);
  }






  updateLevel(idOwn, newLevel) {
    const uri = 'http://localhost:8080/own/' + idOwn;
    let params = new HttpParams();
    params = params.append('level', newLevel);
    return this.http.put(uri, params);
  }
}
