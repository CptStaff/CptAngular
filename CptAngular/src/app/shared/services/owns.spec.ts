import { TestBed, inject } from '@angular/core/testing';

import { OwnsService } from './owns.service';

describe('OwnUserSkillSkillTypeGeneralSkillTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OwnsService]
    });
  });

  it('should be created', inject([OwnsService], (service: OwnsService) => {
    expect(service).toBeTruthy();
  }));
});
