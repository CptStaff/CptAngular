export class User {
    id: number;
    lastname: string;
    firstname: string;
    email: string;
    right: number;
    owns: any = [];
    status: any;
    token: string;
}
