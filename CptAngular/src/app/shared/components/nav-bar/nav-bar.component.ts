import { Router } from '@angular/router';
import { User } from './../../items/user';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'Cpt-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
currentUser: User;


  constructor(public router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
  }
  Disconnect(){
    localStorage.removeItem('currentUser');
  }

}
